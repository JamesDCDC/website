---
title: Animorph
name: Animorph
members: 5
website: https://animorph.coop
email: we@animorph.coop
twitter: animorphcoop
github: 
telephone: 
address: Space4, 149 Fonthill Road,London,N4 3HF
latitude: 51.5635499
longitude: -0.10782689999996364
clients:
- halo-post-production
- southbank-centre
- open-university
- packt
- isd
services:
- animation
- artwork
- augmented-reality
- big-data
- branding-and-identity
- development
- exhibition-and-display
- game-design
- graphic-design
- ideation
- machine-learning
- mixed-reality
- motion-graphics
- virtual-reality
technologies:
- c
- htmlcss
- javascript
- node-js
- python
- unity-3d
---

Animorph addresses unmet medical and social needs using augmented and virtual reality.

We specialise in developing digital medicines and cognitive prosthetics that empower users to achieve what was previously impossible. 

We provide a full range of services and products geared towards bespoke immersive app development, including  research, design workshops, programming, consultation, video production, and sound design.

Since early 2016, we have been collaborating with social enterprises, medical researchers, and organisations including Southbank Centre, The Open University, and Google Innovation Fund.

All stakeholders are invited to actively participate in our development process. We excel at understanding the challenges expressed by our end-users; every story takes us on a unique journey.

Animorph Co-operative is democratically owned and run, with great care for the well-being of our workers,  supporting professional as well as personal development. 

Let’s forge alliances for the future we want to live in.